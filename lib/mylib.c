/*
 * Test library.
 */

#include "mylib-0.h"

/**
 * LibraryObject:
 * @instance_member: A string on an instance.
 *
 * An object in the library.
 */
G_DEFINE_TYPE(LibraryObject, library_object, G_TYPE_OBJECT)

static void
library_object_class_init(LibraryObjectClass *klass)
{
}

static void
library_object_init(LibraryObject *self)
{
}

/**
 * library_object_new:
 *
 * Creates a new library object.
 *
 * Returns: The new library object.
 */
LibraryObject *
library_object_new(void)
{
	return g_object_new(LIBRARY_TYPE_OBJECT, NULL);
}

/**
 * library_object_test:
 * @self: The library object to test.
 *
 * Tests the library object.
 */
void
library_object_test(LibraryObject *self)
{
	LibraryObjectClass *klass;

	g_return_if_fail(LIBRARY_IS_OBJECT(self));

	klass = LIBRARY_OBJECT_GET_CLASS(self);
	g_message("object member: %s, class member: %s",
	          self->instance_member, klass->class_member);
}
