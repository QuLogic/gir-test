#pragma once

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define LIBRARY_TYPE_OBJECT (library_object_get_type())
GType library_object_get_type(void);
typedef struct _LibraryObject LibraryObject;
typedef struct _LibraryObjectClass LibraryObjectClass;

_GLIB_DEFINE_AUTOPTR_CHAINUP(LibraryObject, GObject)

static inline LibraryObject *LIBRARY_OBJECT(gpointer ptr) {
	return G_TYPE_CHECK_INSTANCE_CAST(ptr, library_object_get_type(), LibraryObject);
}
static inline LibraryObjectClass *LIBRARY_OBJECT_CLASS(gpointer ptr) {
	return G_TYPE_CHECK_CLASS_CAST(ptr, library_object_get_type(), LibraryObjectClass);
}
static inline gboolean LIBRARY_IS_OBJECT(gpointer ptr) {
	return G_TYPE_CHECK_INSTANCE_TYPE(ptr, library_object_get_type());
}
static inline gboolean LIBRARY_IS_OBJECT_CLASS(gpointer ptr) {
	return G_TYPE_CHECK_CLASS_TYPE(ptr, library_object_get_type());
}
static inline LibraryObjectClass *LIBRARY_OBJECT_GET_CLASS(gpointer ptr) {
	return G_TYPE_INSTANCE_GET_CLASS(ptr, library_object_get_type(), LibraryObjectClass);
}


struct _LibraryObject {
	/*< private >*/
	GObject parent;

	/*< public >*/
	gchar *instance_member;

	/*< private >*/
	gpointer reserved[4];
};

struct _LibraryObjectClass {
	/*< private >*/
	GObjectClass parent_class;

	/*< public >*/
	gchar *class_member;

	/*< private >*/
	gpointer reserved[4];
};

LibraryObject *library_object_new(void);
void library_object_test(LibraryObject *self);

G_END_DECLS
