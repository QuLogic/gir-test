#pragma once

#include <mylib-0.h>
#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define APP_TYPE_OBJECT (app_object_get_type())
G_DECLARE_FINAL_TYPE(AppObject, app_object, APP, OBJECT, LibraryObject)

struct _AppObject {
	LibraryObject parent;
};

AppObject * app_object_new(void);

G_END_DECLS
