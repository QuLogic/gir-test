public class AppObject : Library.Object {
	public class string class_member = "Vala Class member";

	public AppObject() {
		this.instance_member = "Vala Instance member";
	}
}

int
main()
{
	AppObject obj = new AppObject();

	obj.test();

	return 0;
}
