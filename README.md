GObject-Introspection Test
==========================

This is a repository for testing out
[GObject](https://developer.gnome.org/gobject/stable/) and
[GObject-Introspection](https://gi.readthedocs.io/).

You need Meson, Ninja, and Vala to build. Building uses the normal Meson
method:

    $ meson build
    $ ninja -C build

After building, you should be able to run the test programs from the `build`
directory:

    $ ./build/app
    $ ./build/python/pyapp
    $ ./build/vala/vala-app
