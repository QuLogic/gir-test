#include "app.h"

G_DEFINE_TYPE(AppObject, app_object, LIBRARY_TYPE_OBJECT)

static void
app_object_class_init(AppObjectClass *klass)
{
	LibraryObjectClass *library_class = LIBRARY_OBJECT_CLASS(klass);

	library_class->class_member = "C Class member";
}

static void
app_object_init(AppObject *self)
{
	LibraryObject *library_obj = LIBRARY_OBJECT(self);

	library_obj->instance_member = "C Instance member";
}

AppObject *
app_object_new(void)
{
	return g_object_new(APP_TYPE_OBJECT, NULL);
}

int
main(void)
{
	AppObject *obj = g_object_new(APP_TYPE_OBJECT, NULL);

	library_object_test(LIBRARY_OBJECT(obj));

	return 0;
}
