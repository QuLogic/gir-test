#!/usr/bin/env python3

import gi
gi.require_version('mylib', '0')  # noqa
from gi.repository import mylib
from gi.repository import GObject


class AppObject(mylib.Object):
    __gtype_name__ = 'AppObject'

    class_member = 'Python Class member'

    def __init__(self):
        GObject.Object.__init__(self)
        self.instance_member = 'Python Instance member'


app = AppObject()
app.test()
